(function () {
    'use strict';
    angular.module('tp.form').component('busReportsForm', {
        templateUrl: 'app/tp.form/busReports/busReportsComponent.html',
        controller: BusReportsComponent,
        controllerAs: 'busReportsComponentVm'
    });
    BusReportsComponent.$inject = ['busDataService', '$log'];

    function BusReportsComponent(busDataService, $log) {
        var busReportsComponentVm = this;

        busReportsComponentVm.serverError = false;
        busDataService.getBusData().then(function (response) {
            busReportsComponentVm.busReports = response.data;
        }, function(errorResponse) {
            busReportsComponentVm.serverError = true;
            $log.error(errorResponse);
        });

        busReportsComponentVm.saveNotes = function(busDetail) {
            busDataService.saveBusDetails(busDetail).then(function(response) {
                busReportsComponentVm.saveSuccess = true;
            }, function(errorResponse) {
                busReportsComponentVm.serverError = false;
            })
        };
    }
})();
