(function () {
    'use strict';

    angular.module('tp.filters')
        .filter('boldRoute', boldRoute);

    function boldRoute() {
        return function(busRoute) {
            if(busRoute === 'UNKNOWN') {
                return busRoute;
            }
            return '<b>' + busRoute.substring(0,3) + '</b>' + busRoute.substring(3, busRoute.length);
        }
    }

})();