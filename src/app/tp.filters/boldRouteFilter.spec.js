describe('boldRouteFilter', function () {
    'use strict';

    var filter;

    beforeEach(module('tp.filters'));

    beforeEach(function() {

        inject(function($filter) {
            filter = $filter;
        })
    });

    describe('filter', function() {

        var busRoute = '891 2 1';
        var boldedBusRoute = '<b>891</b> 2 1';

       it('should make the first route bold', function() {
           var result = filter('boldRoute')(busRoute);
           expect(result).toBe(boldedBusRoute);
       });

        it('should not make UNKNOWN bold', function() {
            var result = filter('boldRoute')('UNKNOWN');
            expect(result).toBe('UNKNOWN');
        });
    });
})