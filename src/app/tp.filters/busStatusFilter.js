(function () {
    'use strict';

    angular.module('tp.filters')
        .filter('busStatus', boldRoute);

    function boldRoute() {
        return function(deviationFromTimetable) {
            if(deviationFromTimetable==null || isNaN(deviationFromTimetable)) {
                return "UNKNOWN";
            }
            if(deviationFromTimetable < 0) {
                return "<span class='early'>Early</span>";
            }

            if(deviationFromTimetable >= 0 && deviationFromTimetable <= 300) {
                return "<span class='onTime'>On Time</span>";
            }

            if(deviationFromTimetable > 300) {
                return "<span class='late'>Late</span>";
            }
            return "UNKNOWN";
        }
    }

})();
