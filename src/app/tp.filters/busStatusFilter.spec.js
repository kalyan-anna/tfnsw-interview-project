describe('boldRouteFilter', function () {
    'use strict';

    var filter;
    var earlyStatus = "<span class='early'>Early</span>";
    var onTimeStatus = "<span class='onTime'>On Time</span>";
    var lateStatus = "<span class='late'>Late</span>";

    beforeEach(module('tp.filters'));

    beforeEach(inject(function ($filter) {
        filter = $filter;
    }));

    it('should return early status when deviationFromTimetable is negative', function() {
        var result = filter('busStatus')(-50);
        expect(result).toBe(earlyStatus);
    });

    it('should return on time status when deviationFromTimetable is 0 or less than equals 300', function() {
        var result = filter('busStatus')(0);
        expect(result).toBe(onTimeStatus);

        result = filter('busStatus')(275);
        expect(result).toBe(onTimeStatus);

        result = filter('busStatus')(300);
        expect(result).toBe(onTimeStatus);
    });

    it('should return late status when deviationFromTimetable is more than 300', function() {
        var result = filter('busStatus')(310);
        expect(result).toBe(lateStatus);
    });

    it('should return UNKNOWN for non numeric values or null', function() {
        var result = filter('busStatus')('dummy');
        expect(result).toBe('UNKNOWN');

        result = filter('busStatus')(null);
        expect(result).toBe('UNKNOWN');
    });
})